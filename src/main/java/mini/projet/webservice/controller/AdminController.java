package mini.projet.webservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import mini.projet.webservice.model.Admin;
import mini.projet.webservice.model.Article;
import mini.projet.webservice.repository.AdminRepository;
import mini.projet.webservice.repository.ArticleRepository;

@Controller
public class AdminController {

    @Autowired
    AdminRepository adminRepository;

    @Autowired
    ArticleRepository articleRepository;

    @RequestMapping(value = "/articles", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    @CrossOrigin
    public List<Article> listArticleValide() {
        List<Article> listArticles = null;

        listArticles = articleRepository.getListArticleActu();

        return listArticles;
    }

    @RequestMapping(value = "/validerArticle/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    @CrossOrigin
    public int valideArticle(@PathVariable int id) {
        int result = 0;

        Article article = articleRepository.findById(id).get();
        article.setEtatValidation(1);

        try {
            articleRepository.save(article);
            result = 1;
        } catch (Exception e) {
            // TODO: handle exception
            throw e;
        }

        return result;
    }

    @RequestMapping(value = "/articleNonValide", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    @CrossOrigin
    public List<Article> listArticleNonValide() {
        List<Article> listArticles = null;

        listArticles = articleRepository.getListArticleNonValide();

        return listArticles;
    }

    @RequestMapping(value = "/loginAdmin", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    @CrossOrigin
    public Admin loginAdmin(@RequestParam(name = "email") String email,
            @RequestParam(name = "password") String password) {
        Admin admin = null;

        admin = adminRepository.login(email, password);

        return admin;
    }
}
