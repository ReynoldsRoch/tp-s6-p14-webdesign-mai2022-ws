package mini.projet.webservice.controller;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import mini.projet.webservice.model.Article;
import mini.projet.webservice.model.ArticleImage;
import mini.projet.webservice.model.Client;
import mini.projet.webservice.repository.ArticleImageRepository;
import mini.projet.webservice.repository.ArticleRepository;
import mini.projet.webservice.repository.ClientRepository;

@Controller
public class MyController {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ArticleImageRepository articleImageRepository;

    @RequestMapping(value = "/articles/{id}/images", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    @CrossOrigin
    public ArticleImage getImageArticle(@PathVariable int id) {
        Article article = articleRepository.findById(id).get();
        return articleImageRepository.getArticleImages(article);
    }

    @RequestMapping(value = "/articles", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    @CrossOrigin
    public int createArticle(@RequestParam(name = "titre") String titre,
            @RequestParam(name = "resume") String resume,
            @RequestParam(name = "image") String image,
            @RequestParam(name = "contenu") String contenu,
            @RequestParam(name = "idclient") int idclient) {
        int result = 0;

        Article article = new Article();
        article.setContenu(contenu);
        article.setTitre(titre);
        article.setResume(resume);
        article.setClient(clientRepository.findById(idclient).get());
        article.setDatePublication(LocalDateTime.now());
        
        ArticleImage articleImage = new ArticleImage();
        try {
            articleRepository.save(article);

            articleImage.setArticle(article);
            articleImage.setImg(image);
            
            articleImageRepository.save(articleImage);

            result = 1;
        } catch (Exception e) {
            // TODO: handle exception
            throw e;
        }

        return result;
    }

    @RequestMapping(value = "/articles/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    @CrossOrigin
    public Article getArticle(@PathVariable int id) {
        return articleRepository.findById(id).get();
    }

    @CrossOrigin
    @RequestMapping(value = "/clients", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int createClient(
            @RequestParam(name = "nom", value = "") String nom,
            @RequestParam(name = "prenom", value = "") String prenom,
            @RequestParam(name = "email", value = "") String email,
            @RequestParam(name = "password", value = "") String password,
            @RequestParam(name = "dtn", value = "") String dtn) {

        int result = 0;
        Client client = new Client();

        try {
            client.setDtn(Date.valueOf(dtn));
            client.setNom(nom);
            client.setPrenom(prenom);
            client.setMail(email);
            client.setPassword(password);
            clientRepository.save(client);
            result = 1;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return result;
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    @CrossOrigin
    public Client getClient(@PathVariable int id) {
        return clientRepository.findById(id).get();
    }

    @CrossOrigin
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Client logClient(@RequestParam(name = "email", value = "") String email,
            @RequestParam(name = "password", value = "") String password) {
        Client client = null;

        client = clientRepository.login(email, password);

        return client;
    }

    @CrossOrigin
    @RequestMapping(value = "/clients/{id}/articles", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Article> articleClients(@PathVariable int id) {

        List<Article> result = null;

        // Client client = clientRepository.findById(id).get();
        Client client = clientRepository.findById(id).orElse(null);

        if (client != null) {
            result = articleRepository.getListArticle(client);
        }

        return result;
    }

    @RequestMapping(value = "/selectClient", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Client> selectClient() {
        return clientRepository.findAll();
    }

    @RequestMapping(value = "/message", method = RequestMethod.GET)
    @ResponseBody
    public String message() {
        return "hi!";
    }
}
