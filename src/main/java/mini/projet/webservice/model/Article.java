package mini.projet.webservice.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String titre;
    private String resume;
    private String contenu;

    @Column(name = "datepublication")
    private LocalDateTime datePublication;

    @ManyToOne
    @JoinColumn(name = "idclient")
    private Client client;

    @Column(name = "etatvalidation")
    private int etatValidation;

    @Override
    public String toString() {
        return "Article [id=" + id + ", titre=" + titre + ", resume=" + resume + ", contenu=" + contenu
                + ", datePublication=" + datePublication + ", client=" + client + ", etatValidation=" + etatValidation
                + "]";
    }

}
