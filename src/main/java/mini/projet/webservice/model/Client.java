package mini.projet.webservice.model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Entity
// @Data
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nom;
    private String prenom;
    private Date dtn;
    private String mail;
    private String password;

    // @OneToMany(fetch = FetchType.EAGER, mappedBy = "client", cascade =
    // CascadeType.ALL)
    // // @OneToMany(mappedBy = "client")
    // private List<Article> listArticle;

    // public List<Article> getListArticle() {
    // return listArticle;
    // }

    // public void setListArticle(List<Article> listArticle) {
    // this.listArticle = listArticle;
    // }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDtn() {
        return dtn;
    }

    public void setDtn(Date dtn) throws Exception {

        this.dtn = dtn;

        int dtnYear = dtn.toLocalDate().getYear();
        int yearNow = LocalDateTime.now().getYear();
        // System.out.println("now : " + yearNow);
        // System.out.println("dtn year : " + dtnYear);
        if (yearNow - dtnYear < 18) {
            throw new Exception("DateDeNaissance invalide");
        }

    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) throws Exception {
        try {
            this.password = hash(password);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw e;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Client [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dtn=" + dtn + ", mail=" + mail
                + ", password=" + password + "]";
    }

    public static String hash(String input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] messageDigest = md.digest(input.getBytes());
        StringBuilder sb = new StringBuilder();
        for (byte b : messageDigest) {
            sb.append(String.format("%02x", b & 0xff));
        }
        return sb.toString();
    }
}
