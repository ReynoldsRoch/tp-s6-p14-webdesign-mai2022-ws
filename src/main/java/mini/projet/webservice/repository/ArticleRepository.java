package mini.projet.webservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mini.projet.webservice.model.Article;
import mini.projet.webservice.model.Client;

public interface ArticleRepository extends JpaRepository<Article, Integer> {

    @Query("SELECT a FROM Article a WHERE etatvalidation=1 order by datepublication desc")
    List<Article> getListArticleActu();

    @Query("SELECT a FROM Article a WHERE etatvalidation=0 order by datepublication desc")
    List<Article> getListArticleNonValide();

    @Query("SELECT a FROM Article a WHERE a.client = ?1 and etatvalidation=1 order by datepublication desc")
    List<Article> getListArticle(Client client);

}
