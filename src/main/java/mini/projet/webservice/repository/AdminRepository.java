package mini.projet.webservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mini.projet.webservice.model.Admin;

public interface AdminRepository extends JpaRepository<Admin, Integer> {

    @Query("select a from Admin a where email=?1 and password=md5(?2)")
    Admin login(String email, String password);
}
