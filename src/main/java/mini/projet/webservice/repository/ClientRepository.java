package mini.projet.webservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import mini.projet.webservice.model.Client;

public interface ClientRepository extends JpaRepository<Client, Integer> {

    // @Query("SELECT c FROM CLIENT c where c.mail= :email and c.password=
    // md5(:password)")
    @Query("SELECT c FROM Client c where c.mail= ?1 and c.password= md5(?2)")
    Client login(String email, String password);
}
