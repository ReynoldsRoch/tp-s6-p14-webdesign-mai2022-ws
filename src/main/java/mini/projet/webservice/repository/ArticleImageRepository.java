package mini.projet.webservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mini.projet.webservice.model.Article;
import mini.projet.webservice.model.ArticleImage;

public interface ArticleImageRepository extends JpaRepository<ArticleImage, Integer> {
    @Query("select a from ArticleImage a where a.article=?1")
    ArticleImage getArticleImages(Article article);
}
